<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$res = [];

$res['STATUS'] = ( isset($arResult['STATUS']) )? $arResult['STATUS'] : 500;
$res['ERRORS'] = $arResult['ERRORS'];
$res['DEBUG'] = [
	'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
	'FILE' => basename(__FILE__, '.php'),
];
echo json_encode($res);