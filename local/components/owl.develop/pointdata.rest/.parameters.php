<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"PARAMETERS" => array(
		'BLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => "HBLOCK ID",
			'TYPE' => 'TEXT'
		),
		"VARIABLE_ALIASES" => Array(
			"ELEMENT_ID" => Array("NAME" => "ELEMENT_ID"),
		),
		"SEF_MODE" => Array(
			"list" => array(
				"NAME" => "Список",
				"DEFAULT" => "",
				"VARIABLES" => array(),
			),
			"add" => array(
				"NAME" => "Добавить",
				"DEFAULT" => "add",
				"VARIABLES" => array(),
			),
			"id" => array(
				"NAME" => "Действия с элементом",
				"DEFAULT" => "id/#ELEMENT_ID#",
				"VARIABLES" => array("ELEMENT_ID"),
			),
		),
	),
);