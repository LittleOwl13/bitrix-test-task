<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$res = [];

$inputData = file_get_contents('php://input');
if($inputData){
	$inputData = json_decode($inputData, true);
}

if ($inputData['TOKEN'] == $NEEDLY_TOKEN) {
	
	$data_errors = [];
	
	$data = [];
	foreach($fields as $f_key => $f){
		$input_f_key = mb_strtolower(substr($f_key, 3));
		if(
			($f_key == 'UF_UPDATED_AT')
			|| ($f_key == 'UF_CREATED_AT')
		){
			$data[$f_key] = ( isset($inputData[$input_f_key]) )? $inputData[$input_f_key] : date('d.m.Y H:i:s');
			$data[$f_key] = new \Bitrix\Main\Type\DateTime($data[$f_key]);
			continue;
		}
		
		if( ($f['MANDATORY'] == 'Y') && (empty($inputData[$input_f_key])) ) {
			$data_errors[] = 'DATA '.$f_key.' IS REQUIRED';
		}else{
			$data[$f_key] = $inputData[$input_f_key];
		}
	}
		
	if( empty($data_errors) ){
		$result = $entity_data_class::add($data);
		if($result->isSuccess()){
			$res['STATUS'] = 200;
			// $res['DEBUG'] = [
				// 'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
				// 'FILE' => basename(__FILE__, '.php'),
				// 'INPUT_DATA' => $inputData,
				// 'FIELDS' => $fields,
				// 'ELEMENT_DATA' => $data,
			// ];
		}else{
			$res['STATUS'] = 500;
			$res['ERRORS'] = [];
			$res['ERRORS'][] = $result->getErrorMessages();
		}
		
	}else{
		$res['STATUS'] = 500;
		$res['ERRORS'] = $data_errors;
	}
	
	
	
}else{
	$res['STATUS'] = 401;
	$res['ERRORS'] = [];
	$res['ERRORS'][] = 'YOU MUST BE ADMIN';
}

echo json_encode($res);