<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

use Bitrix\Main\Loader; 
Loader::includeModule("highloadblock");


use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$NEEDLY_TOKEN = '999'; // any token for admin access
// hlblock info
$hlblock_id = $arParams['BLOCK_ID'];
if (empty($hlblock_id))
{
	ShowError(GetMessage('HLBLOCK_LIST_NO_ID'));
	return 0;
}
$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
if (empty($hlblock))
{
	ShowError(GetMessage('HLBLOCK_LIST_404'));
	return 0;
}

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

// uf info
global $USER_FIELD_MANAGER;

$fields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);
$arDefaultUrlTemplates404 = [
	'list'    => '',
	'add' => 'add',
	'id' => 'id/#ELEMENT_ID#',
];

$arDefaultVariableAliases404 = [];
$arDefaultVariableAliases    = [];
$arComponentVariables        = ['ELEMENT_ID'];
$SEF_FOLDER                  = '';
$arUrlTemplates              = [];

if ($arParams['SEF_MODE'] == 'Y') {
	
	$arVariables = [];
	
	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
		$arDefaultUrlTemplates404,
		$arParams['SEF_URL_TEMPLATES']
	);
	
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
		$arDefaultVariableAliases404,
		$arParams['VARIABLE_ALIASES']
	);
	
	$engine = new CComponentEngine($this);
	
	$componentPage = $engine->guessComponentPath(
		$arParams["SEF_FOLDER"],
		$arUrlTemplates,
		$arVariables
	);

	$b404 = false;
	if(!$componentPage)
	{
		$componentPage = "list";
		$b404 = true;
	}
	
	
	CComponentEngine::InitComponentVariables(
		$componentPage,
		$arComponentVariables,
		$arVariableAliases,
		$arVariables
	);
	
	$SEF_FOLDER = $arParams['SEF_FOLDER'];
}else{
	// NOTHING! Only SEF_MODE for test task
}

$arResult = [
	'FOLDER'        => $SEF_FOLDER,
	'URL_TEMPLATES' => $arUrlTemplates,
	'VARIABLES'     => $arVariables,
	'ALIASES'       => $arVariableAliases,
];

if(($componentPage == 'add') && ($_SERVER['REQUEST_METHOD'] != 'POST')){
	$componentPage = 'error';
	$arResult['ERRORS'] = [];
	$arResult['ERRORS'][] = 'WRONG METHOD';
}else if(($componentPage == 'list') && ($_SERVER['REQUEST_METHOD'] != 'GET')){
	$componentPage = 'error';
	$arResult['ERRORS'] = [];
	$arResult['ERRORS'][] = 'WRONG METHOD';
}

include($componentPage.'.php');