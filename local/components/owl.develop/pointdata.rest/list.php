<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$res = [];

$result = $entity_data_class::getList([
	'select' => ['*'],
	'order' => ['ID' => 'DESC'],
]);

$arElementResult = [];
while ($arRow = $result->Fetch()) {
	$elem = [];
	
	foreach($arRow as $key => $val){
		if($key == 'UF_UPDATED_AT') {
			if($val){
				$elem['updated_at'] = $val->toString();
			}else{
				$elem['updated_at'] = '';
			}
			continue;
		}
		if($key == 'UF_CREATED_AT') {
			if($val){
				$elem['created_at'] = $val->toString();
			}else{
				$elem['created_at'] = '';
			}
			continue;
		}
		if($key == 'ID') {
			$elem['id'] = $val;
			continue;
		}
		
		$elem[mb_strtolower(substr($key, 3))] = $val;
	}
	$arElementResult[] = $elem;
}

$res = $arElementResult;

echo json_encode($res);