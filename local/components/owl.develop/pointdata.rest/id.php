<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$res = [];


$ID = intval($arResult['VARIABLES']['ELEMENT_ID']);

$inputData = file_get_contents('php://input');
if($inputData){
	$inputData = json_decode($inputData, true);
}

if($ID <= 0){
	$res['STATUS'] = 500;
	$res['ERRORS'] = [];
	$res['ERRORS'][] = 'WRONG ID';
}else{
	
	switch($_SERVER['REQUEST_METHOD']){
		case 'PUT':
			// UPDATE
			if ($inputData['TOKEN'] == $NEEDLY_TOKEN) {
				
				$data_errors = [];
	
				$data = [];
				foreach($fields as $f_key => $f){
					$input_f_key = mb_strtolower(substr($f_key, 3));
					
					if(
						$f_key == 'UF_UPDATED_AT'
					){
						$data[$f_key] = ( isset($inputData[$input_f_key]) )? $inputData[$input_f_key] : date('d.m.Y H:i:s');
						$data[$f_key] = new \Bitrix\Main\Type\DateTime($data[$f_key]);
						continue;
					}
					if(
						($f_key == 'UF_CREATED_AT') && isset($inputData[$input_f_key])
					){
						$data[$f_key] = $inputData[$input_f_key];
						$data[$f_key] = new \Bitrix\Main\Type\DateTime($data[$f_key]);
						continue;
					}
					
					if( ($f['MANDATORY'] == 'Y') && (isset($inputData[$input_f_key])) && (empty($inputData[$input_f_key])) ) {
						$data_errors[] = 'DATA '.$f_key.' IS REQUIRED';
					}else{
						if(isset($inputData[$input_f_key])){
							$data[$f_key] = $inputData[$input_f_key];
						}
					}
				}
				
				if( empty($data_errors) ){
					$result = $entity_data_class::update($ID, $data);
					if($result->isSuccess()){
						$res['STATUS'] = 200;
						// $res['DEBUG'] = [
							// 'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
							// 'FILE' => basename(__FILE__, '.php'),
							// 'INPUT_DATA' => $inputData,
							// 'FIELDS' => $fields,
							// 'ELEMENT_DATA' => $data,
						// ];
					}else{
						$res['STATUS'] = 500;
						$res['ERRORS'] = [];
						$res['ERRORS'][] = $result->getErrorMessages();
					}
					
				}else{
					$res['STATUS'] = 500;
					$res['ERRORS'] = $data_errors;
				}
				
				
				
				
				$res['STATUS'] = 200;
				// $res['DEBUG'] = [
					// 'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
					// 'FILE' => basename(__FILE__, '.php'),
					// 'INPUT_DATA' => $inputData,
				// ];
				
			}else{
				$res['STATUS'] = 401;
				$res['ERRORS'] = [];
				$res['ERRORS'][] = 'YOU MUST BE ADMIN';
			}
			
		break;
		case 'DELETE':
			// DELETE
			if ($inputData['TOKEN'] == $NEEDLY_TOKEN) {
				$result = $entity_data_class::delete($ID);
				
				if($result->isSuccess()){
					$res['STATUS'] = 200;
					// $res['DEBUG'] = [
						// 'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
						// 'FILE' => basename(__FILE__, '.php'),
						// 'INPUT_DATA' => $inputData,
						// 'FIELDS' => $fields,
						// 'ELEMENT_DATA' => $data,
					// ];
				}else{
					$res['STATUS'] = 500;
					$res['ERRORS'] = [];
					$res['ERRORS'][] = $result->getErrorMessages();
				}
				
			}else{
				$res['STATUS'] = 401;
				$res['ERRORS'] = [];
				$res['ERRORS'][] = 'YOU MUST BE ADMIN';
			}
		break;
		case 'GET':
			// GET
			$result = $entity_data_class::getList([
				'select' => ['*'],
				'order' => ['ID' => 'DESC'],
				'filter' => ['ID' => $ID],
			]);
			
			$elementResult = [];
			while ($arRow = $result->Fetch()) {
				foreach($arRow as $key => $val){
					if($key == 'UF_UPDATED_AT') {
						if($val){
							$elementResult['updated_at'] = $val->toString();
						}else{
							$elementResult['created_at'] = '';
						}
						continue;
					}
					if($key == 'UF_CREATED_AT') {
						if($val){
							$elementResult['created_at'] = $val->toString();
						}else{
							$elementResult['created_at'] = '';
						}
						continue;
					}
					if($key == 'ID') {
						$elementResult['id'] = $val;
						continue;
					}
					
					$elementResult[mb_strtolower(substr($key, 3))] = $val;
				}

			}
			if(!empty($elementResult)){
				$res = $elementResult;
			}else{
				$res['STATUS'] = 404;
				$res['ERRORS'] = [];
				$res['ERRORS'][] = 'ELEMENT NOT FOUND';
			}
			
		break;
		default:
			// ERROR
			$res['STATUS'] = 500;
			$res['ERRORS'] = [];
			$res['ERRORS'][] = 'WRONG METHOD';
		break;
	}
	
}
echo json_encode($res);