<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
header('Content-Type: application/json');
$APPLICATION->IncludeComponent(
	"owl.develop:pointdata.rest", 
	"", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BLOCK_ID" => "1",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/point-data/",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"add" => "add",
			"id" => "id/#ELEMENT_ID#",
		)
	),
	false
);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');?>