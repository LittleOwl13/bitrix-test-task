Битриксу нужно знать о существовании коплексного компонента, поэтому сначала придётся подключить его в визуальном редакторе
или дописать правило в urlrewrite.php
/////////////////////////////////////
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("1С-Битрикс: Управление сайтом");
?>
<?$APPLICATION->IncludeComponent(
	"owl.develop:pointdata.rest", 
	"", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BLOCK_ID" => "1",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/point-data/",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"add" => "add",
			"id" => "id/#ELEMENT_ID#",
		)
	),
	false
);?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>